#!/bin/bash
#PBS -N pbe_4.54.5
#PBS -q power-chemistry
#PBS -q gpu2 -lngpus=1
#PBS -l walltime=2400:00:00

module load CUDA/CUDA-11.5

cd $PBS_O_WORKDIR

export CUDA_VISIBLE_DEVICES="3"

nep > NEP.log
