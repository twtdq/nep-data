clear;font_size=15;
load force_test.out; 
force100=force_test(1:72*20,:);
force200=force_test(72*20+1:72*40,:);
force300=force_test(72*40+1:72*60,:);
force400=force_test(72*60+1:72*80,:);
force500=force_test(72*80+1:72*100,:);
force600=force_test(72*100+1:72*120,:);
force700=force_test(72*120+1:72*140,:);
force800=force_test(72*140+1:72*160,:);
force900=force_test(72*160+1:72*180,:);
force1000=force_test(72*180+1:72*200,:);
force100nep=reshape(force100(:,1:3),1440*3,1);
force100dft=reshape(force100(:,4:6),1440*3,1);
force200nep=reshape(force200(:,1:3),1440*3,1);
force200dft=reshape(force200(:,4:6),1440*3,1);
force300nep=reshape(force300(:,1:3),1440*3,1);
force300dft=reshape(force300(:,4:6),1440*3,1);
force400nep=reshape(force400(:,1:3),1440*3,1);
force400dft=reshape(force400(:,4:6),1440*3,1);
force500nep=reshape(force500(:,1:3),1440*3,1);
force500dft=reshape(force500(:,4:6),1440*3,1);
force600nep=reshape(force600(:,1:3),1440*3,1);
force600dft=reshape(force600(:,4:6),1440*3,1);
force700nep=reshape(force700(:,1:3),1440*3,1);
force700dft=reshape(force700(:,4:6),1440*3,1);
force800nep=reshape(force800(:,1:3),1440*3,1);
force800dft=reshape(force800(:,4:6),1440*3,1);
force900nep=reshape(force900(:,1:3),1440*3,1);
force900dft=reshape(force900(:,4:6),1440*3,1);
R100=sqrt(mean((force100nep-force100dft).^2))
R200=sqrt(mean((force200nep-force200dft).^2))
R300=sqrt(mean((force300nep-force300dft).^2))
R400=sqrt(mean((force400nep-force400dft).^2))
R500=sqrt(mean((force500nep-force500dft).^2))
R600=sqrt(mean((force600nep-force600dft).^2))
R700=sqrt(mean((force700nep-force700dft).^2))
R800=sqrt(mean((force800nep-force800dft).^2))
R900=sqrt(mean((force900nep-force900dft).^2))

figure;
subplot(2, 2, 1);
plot(force100(:,4:6),force100(:,1:3),'.','markersize',10); hold on;
plot(-20:0.01:20,-20:0.01:20,'k--','linewidth',1);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/\rm $\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-7.5,7.5,    num2str(R100) ,'fontsize',15,'interpreter','latex')
%text(-5.5,7.5,  'eV/$\AA$','fontsize',15,'interpreter','latex')
axis tight;
title('(a)');
subplot(2, 2, 2);
plot(force300(:,4:6),force300(:,1:3),'.','markersize',10); hold on;
plot(-20:0.01:20,-20:0.01:20,'k--','linewidth',1);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/\rm $\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-7.5,7.5,    num2str(R300) ,'fontsize',15,'interpreter','latex')
%text(-5.5,7.5,  'eV/$\AA$','fontsize',15,'interpreter','latex')
axis tight;
title('(b)');
subplot(2, 2, 3);
plot(force500(:,4:6),force500(:,1:3),'.','markersize',10); hold on;
plot(-20:0.01:20,-20:0.01:20,'k--','linewidth',1);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/\rm $\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-7.5,7.5,    num2str(R500) ,'fontsize',15,'interpreter','latex')
%text(-5.5,7.5,  'eV/$\AA$','fontsize',15,'interpreter','latex')
axis tight;
title('(c)');

subplot(2, 2, 4);
plot(force700(:,4:6),force700(:,1:3),'.','markersize',10); hold on;
plot(-20:0.01:20,-20:0.01:20,'k--','linewidth',1);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/\rm $\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-7.5,7.5,    num2str(R700) ,'fontsize',15,'interpreter','latex')
%text(-5.5,7.5,  'eV/$\AA$','fontsize',15,'interpreter','latex')
axis tight;
title('(d)');

figure
plot(force700(:,4:6),force700(:,1:3),'.','markersize',10); hold on;
plot(-20:0.01:20,-20:0.01:20,'k--','linewidth',1);
xlabel('DFT force (eV/$\AA$)','fontsize',15,'interpreter','latex');
ylabel('NEP force (eV/\rm $\AA$)','fontsize',15,'interpreter','latex');
set(gca,'fontsize',15,'ticklength',get(gca,'ticklength')*2);
text(-7.5,7.5,    num2str(R700) ,'fontsize',15,'interpreter','latex')
%text(-5.5,7.5,  'eV/$\AA$','fontsize',15,'interpreter','latex')
axis tight;
title('(d)');
